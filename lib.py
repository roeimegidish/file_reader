import re
from colorama import Fore, Style
from pymongo import MongoClient

def find_in_text_colorized(text,find_array):
    strings_to_search = find_array

    combined_string = "|".join(strings_to_search)

    for match in re.finditer(combined_string, text):
        text = text.replace(match.group(), f"{Fore.CYAN}{match.group()}{Style.RESET_ALL}")

    print(text)

def find_by_mongo(text):
    client = MongoClient('mongodb://medic_user:Medic%234343%25s@192.114.5.161/')
    result = client['medic_app']['medicines'].aggregate([
        {
            '$unwind': {
                'path': '$eng', 
                'preserveNullAndEmptyArrays': True
            }
        }, {
            '$group': {
                '_id': None, 
                'Unique': {
                    '$addToSet': '$eng'
                }
            }
        }
    ])
    obj = list(result)[0]
    find_in_text_colorized(text,obj['Unique'])
